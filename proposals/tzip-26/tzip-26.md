---
tzip: 026
title: FA2.1 - Ticket Asset Interface 
status: Draft
author: Lucas Felli (@Lfelli), Charles Dehlinger (@Charles.d)
type: Financial Application (FA)
created: 2022-08-10
date: 2023-25-09
version: 1.0
---

## Summary

TZIP-026 proposes FA2.1 as an extension of the FA2 standard introduced in [TZIP-012](https://tzip.tezosagora.org/proposal/tzip-12/). FA2.1 adds support for on-chain views, tickets, events, and a finite allowance mechanism similar to FA1.2.

## Abstract

The goal of the FA2.1 extension is to include new protocol features while remaining fully compatible with current applications that rely on the FA2 standard.

**Events** are a means to solve discoverability problems encountered by indexers, i.e. to know when a token is transferred especially outside the specified transfer entrypoint (mint, burn, etc.).

**On-chain views** are a means for smart contracts to share information easily. Callback views are kept for compatibility with already deployed contracts.

**Tickets** are a way to materialize tokens outside of smart contracts. They can be transferred without calling the creator contract and affecting its status. Tezos rollups rely on tickets for representing Layer 1 assets on Layer 2.

Additionally, **finite allowance**, enabling a user to grant permission to spend a fixed amount of tokens, is introduced (based on the FA1.2 scheme).

## General

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

FA2.1 MUST comply with the same rules defined by FA2 standard:

- Token type is uniquely identified on the chain by a pair composed of the token contract address and token ID, a natural number (`nat`). If the underlying contract implementation supports only a single token type (e.g. ERC-20-like contract), the token ID MUST be `0n`. In cases where multiple token types are supported within the same FA2.1 token contract (e.g. ERC-1155-like contract), the contract is fully responsible for assigning and managing token IDs.

- The FA2.1 batch entrypoints accept a list (batch) of parameters describing a single operation or a query. The batch MUST NOT be reordered or deduplicated and MUST be processed in the same order it is received.

- An empty batch is a valid input and MUST be processed as a non-empty one. For example, an empty transfer batch will not affect token balances, but applicable transfer core behavior and permission policy MUST be applied.

- If the underlying contract implementation supports only a single token type, the batch may contain zero or multiple entries where token ID is a fixed `0n` value. Likewise, if multiple token types are supported, the batch may contain zero or more entries and there may be duplicate token IDs.

## Extension specification

Token contracts implementing the FA2.1 standard MUST follow this interface:

- entrypoints:
  - balance_of (for backwards compatibility with FA2)
  - update_operators (for backwards compatibility with FA2)
  - transfer
  - approve
  - export_ticket
  - import_ticket
- views:
  - get_balance
  - get_total_supply
  - is_operator
  - get_allowance
  - get_token_metadata
  - is_token
- events:
  - transfer_event
  - balance_update
  - total_supply_update
  - operator_update
  - allowance_update
  - token_metadata_update

The FA2 entrypoints `balance_of` and `update_operators` should be deprecated at some point, but many applications rely on them so they MUST be implemented for backwards compatibility.

In some specific use cases, `export_ticket` and `import_ticket` may cause conflicts with the transfer policy. For this reason, one can simply throw an "FA2.1_TICKETS_UNSUPPORTED" error without implementing the logic of both entrypoints.

This standard relies on a specific type of ticket: `(pair nat (option bytes))` to represent assets. Be cautious not to reuse this type if you implement another custom ticket functionnality.

### Entrypoint semantics

#### `transfer`

The core behavior is the same as for FA2 with additional rules regarding allowances:

- To avoid conflicts between `approve` and `update_operators`, the operator's rights take precedence over those of the spender.
- If the allowance of the sender falls under zero, the transfer MUST fail  with the `"FA2.1_INSUFFICIENT_ALLOWANCE"` error message.
- By default, transfer of zero amount is allowed but beware of the behavior regarding allowance equals to `0` (or not set at all).

In the case of a self-transfer (`%from_ == %to_`), make sure to not allow one to transfer more than they own, i.e. do not update `%to_`'s balance before `%from_`'s balance or check `%from_`'s balance is enough to transfer `%amount`.

Associated events: transfer_event, balance_update, allowance_update

#### `approve`

```
(list %approve
    (pair
        (address %owner)
        (address %spender)
        (nat %token_id)
        (or (nat %increase)
            (nat %decrease)
        )
    )
)
```

This entrypoint allows to increase or decrease the allowance set to `%spender` regarding a token `%owner` and a `%token_id`. The `%spender` can withdraw multiple times from the `%owner`'s balance up to the set value.

In case of a decrease below `0`, no error is thrown, the allowance MUST be removed or set to `0`. Beware of the case where anyone can transfer a `0` amount of tokens from anyone else because the default allowance is `0`.

Each call of `transfer` and `export_ticket` entrypoints decreases the allowance amount by the transferred amount of tokens, unless the transfer is called with `%from_` being equal to sender or with `%from_` being an operator on behalf of the sender.

The standard does not specify who is permitted to approve on behalf of the token owner. Depending on the business use case, the particular implementation of the FA2.1 contract MAY limit allowance updates to a token owner (`owner == SENDER`) or it can be limited to an administrator.

Associated events: allowance_update

#### `export_ticket`

```
(list %export_ticket
    (pair
        (option %destination 
            (contract 
                (list 
                    (pair
                        address
                        (list (ticket (pair nat (option bytes))))
                    )
                )
            )
        )
        (list
            (pair
                (address %to_)
                (list %tickets_to_export
                    (pair
                       (address %from_)
                       (nat %token_id)
                       (nat %amount)
                    )
                )
            )
        )
    )
)
```

The ticket id MUST be a pair containing the `%token_id` and any additional information (for example: a timestamp, ...) as an `option bytes`. The ticket value MUST be the `%amount`.

The entrypoint MUST comply with the same policy as the `transfer` entrypoint. The balance of `%from_` MUST be decreased by `%amount` for each associated `%token_id`. The balance of `%to_` and `%destination` MUST remain unchanged. The total supply MUST remain unchanged.

In the case of the option `%destination` being:

- `None`, an operation is emitted for each exported ticket to its corresponding `address %to_` and this `address` MUST be typed as a `(contract (ticket (pair nat (option bytes)))`. This allows tz addresses to receive tickets directly. The contract MUST fail with the error mnemonic `"FA2.1_INVALID_DESTINATION"` if an `address %to_` can't be correctly typed.
- `(Some contract)`, only one operation is necessary to transfer the tickets to this contract. The contract parameter `(list (pair address (list (ticket (pair nat (option bytes))))))` MUST be constructed using `(address %to_)` and `(list %tickets_to_export)`.

#### `lambda_export`

```
(pair %lambda_export
    (list %tickets_to_export
        (pair
           (address %from_)
           (nat %token_id)
           (nat %amount)
        )
    )
    (lambda %action
        (list (ticket (pair nat (option bytes))))
        (list operation)
    )
)
```

The ticket id MUST be a pair containing the `%token_id` and any additional information (for example: a timestamp, ...) as an `option bytes`. The ticket value MUST be the `%amount`.

The entrypoint MUST comply with the same policy as the `transfer` entrypoint. The balance of `%from_` MUST be decreased by `%amount` for each associated `%token_id`. The total supply MUST remain unchanged.

Users must use the `%action` lambda to send exported tickets to the desired destination (tz addresses, smart contract entrypoints no matter the arguments, ...).

:warning: **The lambda MUST NOT be executed by the FA2.1 contract itself but transferred to a sandbox contract instead whose sole purpose is to execute the lambda**[^sandbox]. This avoids any security breach that could be caused by the operations resulting from the application of the lambda. Finally, the amount of tez transferred to this entrypoint should be forwarded to the sandbox as operations generated by the lambda may use it.

Associated events: balance_update, allowance_update

#### `import_ticket`

```
(list %import_ticket
    (pair
        address %to_
        (list %tickets
            (ticket (pair nat (option bytes)))
        )
    )
)
```

The nat value of the ticket id of type `(pair nat (option bytes))` represents the `token_id` and the ticket value represents the amount to be credited to the `to_` address for the corresponding `token_id`. The ticket SHOULD be destroyed upon import but some use cases may need to store or send them elsewhere.

If the ticket has not been created by the contract in which it is imported (`ticketer != SELF_ADDRESS`), the entrypoint SHOULD fail but some use cases may accept to import tickets minted by other contracts. In case of failure, the error mnemonic MUST be `"FA2.1_INVALID_TICKET"`.

The total supply SHOULD remain unchanged but some use cases may accept tickets from other contracts, for example, and therefore require an increase in the total supply.

Associated events: balance_update, total_supply_update

### Views

To let the view caller implement its own error handling logic, it's recommended not to fail inside a view but rather responding with a meaningful default value to let the callers choose their actions. For example, returning 0 when trying to get the balance of a non-existing wallet or a non-existing token id.

#### `get_balance`

Returns the number of tokens `%token_id` held for each `%owner` in the `%requests` list.

If `%owner` isn't known by the contract or `%token_id` doesn't exist, the default returned value SHOULD be `0`.

```
view "get_balance" 
    (pair
      (address %owner)
      (nat %token_id)
    )
    (nat %balance)
```

#### `get_total_supply`

Returns the total supply of `token_id`. The total supply accounts for the number of tokens stored by the contract and the ones exported in tickets. i.e., exporting tickets MUST not change the total supply.

If `%token_id` doesn't exist, the default returned value SHOULD be `0`.

```
view "get_total_supply" 
    (nat %token_id)
    (nat %total_supply)
```

#### `is_operator`

Returns true if `%operator` is an operator of `%owner` for the token `%token_id`.

If `%owner` or `%operator` isn't known by the contract or `%token_id` doesn't exist, the default returned value SHOULD be `False`.

```
view "is_operator" 
    (pair
      (address %owner)
      (address %operator)
      (nat %token_id)
    )
    (bool %is_operator)
```

#### `get_allowance`

Returns the number of tokens `%token_id` allowed by `%owner` for `%spender`.

If `%owner` or `%spender` isn't known by the contract or `%token_id` doesn't exist, the default returned value SHOULD be `0`.

```
view "get_allowance" 
    (pair
      (address %owner)
      (address %spender)
      (nat %token_id)
    )
    (nat %allowance)
```

#### `get_token_metadata`

Returns the metadata associated to `%token_id`.

If `%token_id` doesn't exist, the default returned value SHOULD be an empty `map %token_info`.

```
view "get_token_metadata" 
    (nat %token_id)
    (map %token_info string bytes)
```

#### `is_token`

Returns `True` if `%token_id` exists and `False` otherwise.

```
view "is_token" 
    (nat %token_id)
    (bool %token_exist)
```

### Events

To avoid confusion about the order of events, they MUST be emitted before further internal transactions if any.

#### `transfer_event`

MUST be triggered when a transfer occurs by any mechanism (a call to `transfer` but also custom entrypoints implementing a concept of transfer).

In particular for the `transfer` entrypoint, each element of the list MUST trigger an event.

The entrypoint `export_ticket` and `import_ticket` SHOULD NOT trigger this event.

```
(pair %transfer_event
    (address %from_)
    (address %to_)
    (nat %token_id)
    (nat %amount)
)
```

Associated entrypoints: transfer

#### `balance_update`

MUST be triggered when the balance of a token owner is modified by any mechanism.

Only one `balance_update` event is allowed per pair (`%owner`, `%token_id`) per transaction.

`int %diff` represents the difference between `nat %new_balance` and the previous value (`new_balance = old_balance + diff`).

```
(pair %balance_update
    (address %owner)
    (nat %token_id)
    (nat %new_balance)
    (int %diff)
)
```

Associated entrypoints: transfer, export_ticket, lambda_export, import_ticket

#### `total_supply_update`

MUST be triggered when the total supply of a `%token_id` is modified by any mechanism.

`int %diff` represents the difference between `nat %new_total_supply` and the previous value (`new_total_supply = old_total_supply + diff`). So a positive `%diff` is a mint and a negative `%diff` is a burn.

Only one `total_supply_update` event is allowed per `%token_id` per transaction.

```
(pair %total_supply_update
    (nat %token_id)
    (nat %new_total_supply)
    (int %diff)
)
```

Associated entrypoints: import_ticket, mint (non standard)

#### `operator_update`

MUST be triggered when an operator is updated by any mechanism.

Only one `operator_update` event is allowed per triplet (`%owner`, `%operator`, `%token_id`) per transaction.

```
(pair %operator_update
    (address %owner)
    (address %operator)
    (nat %token_id)
    (bool %is_operator)
)
```

`%is_operator` is true if `%operator` has been set as an operator of `owner` and false otherwise.

Associated entrypoints: update_operators

#### `allowance_update`

MUST be triggered when a spender's allowance is updated by any mechanism (including when the allowance is consumed by a call to `transfer`).

`int %diff` represents the difference between `nat %new_allowance` and the previous value (`new_allowance = old_allowance + diff`).

Only one `allowance_update` event is allowed per triplet (`%owner`, `%spender`, `%token_id`) per transaction.

```
(pair %allowance_update
    (address %owner)
    (address %spender)
    (nat %token_id)
    (nat %new_allowance)
    (int %diff)
)
```

Associated entrypoints: approve, transfer, export_ticket, lambda_export

#### `token_metadata_update`

MUST be triggered when a token metadata is updated by any mechanism, for example mint, burn, reveal, etc.

In case of the token is burned the `option %new_metadata (map string bytes)` MUST be `none`.

Only one `token_metadata_update` event is allowed per `%token_id` per transaction.

```
(pair %token_metadata_update
    (nat %token_id)
    (option %new_metadata (map string bytes))
)
```

Associated entrypoints: mint (non standard)

### Error Handling

Following FA2 pattern:

| Error mnemonic                   | Description                                                                                           |
| :------------------------------- | :---------------------------------------------------------------------------------------------------- |
| `"FA2.1_INVALID_TICKET"`         | A ticket import failed because one of the tickets doesn't belong to the contract (`ticketer != SELF_ADDRESS`)  |
| `"FA2.1_INVALID_DESTINATION"`    | A `%destination` address or contract can't be correctly typed to receive the exported tickets
| `"FA2.1_TICKETS_UNSUPPORTED"`    | Ticket export/import isn't supported                                                 |
| `"FA2.1_INSUFFICIENT_ALLOWANCE"` | A token spender does not have sufficient allowance to transfer tokens from the owner's account           |

### Token metadata as an extension of [TZIP-21](https://tzip.tezosagora.org/proposal/tzip-21/)

`tokenKind` (string)
One of the following "fungible", "semi-fungible" or "non-fungible".

[^sandbox]: Michelson code of the sandbox
    ```
    storage unit;
    parameter (pair
                    (list (ticket (pair nat (option bytes))))
                    (lambda
                        (list (ticket (pair nat (option bytes))))
                        (list operation)));
    code { UNPAIR;
           UNPAIR;
           EXEC;
           PAIR}
    ```
